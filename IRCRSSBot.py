from __future__ import unicode_literals, division, absolute_import, with_statement

import queue
import threading
import os
import re
from threading import Thread
import logging
import time
import feedparser as fp
import webbrowser
from datetime import datetime, timezone
from tkinter import *
from tkinter import messagebox
import sys
from irc.bot import SingleServerIRCBot
from PyQt5 import Qt
import winsound
import subprocess as sub

# p = sub.Popen('IRCnRSSBot2.py', stdout=sub.PIPE, stderr=sub.PIPE)
# output, errors = p.communicate()

site = "SiteNameHere"


class StdoutRedirector(object):
    def __init__(self, text_widget):
        self.text_space = text_widget

    def write(self, string):
        self.text_space.insert('end', string)
        self.text_space.see('end')

    def flush(self):
        pass

def combine_funcs(*funcs):
    def combined_func(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return combined_func


def parse():
    print("Parsing")
    global abrssparse
    abrssparse = fp.parse('ENTER RSS FEED')


def refresh():
    print("Refreshing")
    parse()
    abEntry1Label.config(text=abrssparse.entries[0].title)
    abEntry1Time.config(text=abrssparse.entries[0].published)
    abEntry1Button.config(command=lambda: webbrowser.open(abrssparse.entries[0].link))
    abEntry2Label.config(text=abrssparse.entries[1].title)
    abEntry2Time.config(text=abrssparse.entries[1].published)
    abEntry2Button.config(command=lambda: webbrowser.open(abrssparse.entries[1].link))
    abEntry3Label.config(text=abrssparse.entries[2].title)
    abEntry3Time.config(text=abrssparse.entries[2].published)
    abEntry3Button.config(command=lambda: webbrowser.open(abrssparse.entries[2].link))
    abEntry4Label.config(text=abrssparse.entries[3].title)
    abEntry4Time.config(text=abrssparse.entries[3].published)
    abEntry4Button.config(command=lambda: webbrowser.open(abrssparse.entries[3].link))
    abEntry5Label.config(text=abrssparse.entries[4].title)
    abEntry5Time.config(text=abrssparse.entries[4].published)
    abEntry5Button.config(command=lambda: webbrowser.open(abrssparse.entries[4].link))


class GUI:
    def __init__(self):
        self.tk = Tk()
        parse()
        self.draw()
        self.clock()
        self.tk.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.tk.iconbitmap(default='ENTER ICO NAME')
        self.tk.mainloop()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.tk.destroy()

    def clock(self):
        self.tk.wm_title("Current time: " + datetime.now(timezone.utc).strftime("%X"))
        self.tk.after(1000, self.clock)

    def draw(self):

        global abEntry1Time
        global abEntry1Label
        global abEntry1Button
        global abEntry2Button
        global abEntry2Label
        global abEntry2Time
        global abEntry3Button
        global abEntry3Label
        global abEntry3Time
        global abEntry4Button
        global abEntry4Label
        global abEntry4Time
        global abEntry5Button
        global abEntry5Label
        global abEntry5Time

        #Timer
        self.tk.wm_title("Current time: " + datetime.now(timezone.utc).strftime("%X"))

        #GUI elements code
        consoleOutputLabel = Label(self.tk, text="Console Output")
        siteLabel1 = Label(self.tk, text="REDACTED")
        siteLabel2 = Label(self.tk, text="REDACTED")
        siteLabel3 = Label(self.tk, text="REDACTED")
        abEntry1Label = Label(self.tk, text=abrssparse.entries[0].title)
        abEntry1Time = Label(self.tk, text=abrssparse.entries[0].published)
        abEntry1Button = Button(self.tk, text="Link", command=lambda: webbrowser.open(abrssparse.entries[0].link))
        abEntry2Label = Label(self.tk, text=abrssparse.entries[1].title)
        abEntry2Time = Label(self.tk, text=abrssparse.entries[1].published)
        abEntry2Button = Button(self.tk, text="Link", command=lambda: webbrowser.open(abrssparse.entries[1].link))
        abEntry3Label = Label(self.tk, text=abrssparse.entries[2].title)
        abEntry3Time = Label(self.tk, text=abrssparse.entries[2].published)
        abEntry3Button = Button(self.tk, text="Link", command=lambda: webbrowser.open(abrssparse.entries[2].link))
        abEntry4Label = Label(self.tk, text=abrssparse.entries[3].title)
        abEntry4Time = Label(self.tk, text=abrssparse.entries[3].published)
        abEntry4Button = Button(self.tk, text="Link", command=lambda: webbrowser.open(abrssparse.entries[3].link))
        abEntry5Label = Label(self.tk, text=abrssparse.entries[4].title)
        abEntry5Time = Label(self.tk, text=abrssparse.entries[4].published)
        abEntry5Button = Button(self.tk, text="Link", command=lambda: webbrowser.open(abrssparse.entries[4].link))

        global consoleOutput

        consoleOutput = Text(self.tk, wrap="word")
        consoleOutput.tag_configure("stderr", foreground="#b22222")
        sys.stdout = StdoutRedirector(consoleOutput)
        # wid = consoleOutput.winfo_id()
        # os.system('xterm -into %d -geometry 80x20 -sb -e python &' % wid)
        # consoleOutput.insert(END, output)

        #IRCBot Button code
        t = Thread(name='IRCBot', target=lambda: IRCBot().start())
        t.setDaemon(True)
        startIrcBot = Button(self.tk, text="Start IRC Bot", command=lambda: combine_funcs(t.start(), startIrcBot.config(state=DISABLED), startIrcBot.config(text="IRC Bot Running")))

        #Positioning code
        abEntry1Label.grid(row=1, column=0)
        abEntry1Time.grid(row=2, column=0)
        abEntry1Button.grid(row=3, column=0)
        abEntry2Label.grid(row=4, column=0)
        abEntry2Time.grid(row=5, column=0)
        abEntry2Button.grid(row=6, column=0)
        abEntry3Label.grid(row=7, column=0)
        abEntry3Time.grid(row=8, column=0)
        abEntry3Button.grid(row=9, column=0)
        abEntry4Label.grid(row=10, column=0)
        abEntry4Time.grid(row=11, column=0)
        abEntry4Button.grid(row=12, column=0)
        abEntry5Label.grid(row=13, column=0)
        abEntry5Time.grid(row=14, column=0)
        abEntry5Button.grid(row=15, column=0)
        startIrcBot.grid(row=18, column=0)
        siteLabel1.grid(row=0, column=0)
        consoleOutput.grid(row=1, column=2, rowspan=18)
        consoleOutputLabel.grid(row=0, column=2)
        # siteLabel2.grid(row=0, column=1)
        # siteLabel3.grid(row=0, column=2)


class IRCBot(SingleServerIRCBot, threading.Thread):
    MESSAGE_CLEAN = re.compile("\x0f|\x1f|\x02|\x03(?:[\d]{1,2}(?:,[\d]{1,2})?)?", re.MULTILINE | re.UNICODE)
    URL_MATCHER = re.compile(r'(https?://[\da-z\.-]+\.[a-z\.]{2,6}[/\w\.-\?&]*/?)', re.MULTILINE | re.UNICODE)

    def __init__(self):
        threading.Thread.__init__(self)
        self.queue = queue
        self.server = 'IRC HOSTNAME HERE'
        self.channel = 'IRC ANNOUNCE CHANNEL HERE'
        self.announcer = 'IRC ANNOUNCER BOT NAME HERE'
        self.ignore_lines = []
        self.message_regex = []

        print('Servers: %s' % self.server)
        print('Channels: %s' % self.channel)
        print('Announcers: %s' % self.announcer)
        print('Ignore Lines: %d' % len(self.ignore_lines))
        print('Message Regexs: %d' % len(self.message_regex))

        # Init the IRC Bot
        server = self.server
        port = 6667
        nickname = 'YOUR NICKNAME HERE'
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.stop_bot = False
        self.entry_queue = []
        self.line_cache = {}

    def start_interruptable(self, timeout=0.2):
        """
        Start the IRC bot
        :param timeout: Maximum wait time between process cycles
        :return:
        """
        self._connect()
        print("Connecting")
        # while not self.stop_bot:
        #     print("here8")
        #     self.reactor.process_once(timeout)
        # self.disconnect()
        # print("Disconnecting")

    def parse_message(self, nickname, channel, message):
        """
        Parses a public message and generates an entry if needed
        :param nickname: Nickname of who sent the message
        :param channel: Channel where the message was receivied
        :param message: Message text
        :return:
        """

        # Clean up the message
        message = self.MESSAGE_CLEAN.sub('', message)

        # If we have announcers defined, ignore any messages not from them
        if nickname != self.announcer:
            print('Ignoring message: from non-announcer %s' % nickname)
            return

        # If its listed in ignore lines, skip it
        for rx in self.ignore_lines:
            if rx.match(message):
                print('Ignoring message: matched ignore line')
                return

    def on_welcome(self, conn, irc_event):
        print('IRC connected to %s' % self.connection.get_server_name())
        time.sleep(5)
        # IRC.request_channel_invite()
        self.connection.execute_delayed(1, self.identify_with_nickserv)
        # self.connection.execute_delayed(1)

    def identify_with_nickserv(self):
        """
        Identifies the connection with Nickserv, ghosting to recover the nickname if required
        :return:
        """
        nickserv_password = 'YOUR NICKSERV PASS HERE'
        if nickserv_password:
            # If we've not got our peferred nickname and NickServ is configured, ghost the connection
            if self.connection.get_nickname() != self._nickname:
                print('Ghosting old connection')
                self.connection.privmsg('NickServ', 'GHOST %s %s' % (self._nickname, nickserv_password))
                self.connection.nick(self._nickname)
            # Identify with NickServ
            print('Identifying with NickServ as %s' % self._nickname)
            self.connection.privmsg('NickServ', 'IDENTIFY %s %s' % (self._nickname, nickserv_password))
            time.sleep(2)
            self.connection.execute_delayed(5, self.request_channel_invite)
            # self.connection.execute_delayed(5, self.join_channels)

    def request_channel_invite(self):
        """
        Requests an invite from the configured invite user.
        :return:
        """
        invite_nickname = 'INVITER NICKNAME HERE'
        invite_message = 'PRIVATE INVITE MESSAGE KEY HERE'
        print('Requesting an invite to channels from %s' % invite_nickname)
        self.connection.privmsg(invite_nickname, invite_message)
        self.connection.execute_delayed(5, self.join_channels)

    def join_channels(self):
        """
        Joins any channels configured if not already in them
        :return:
        """
        # Queue to join channels, we want to wait for NickServ to work before joining
        print("Attempting to join channel")
        self.connection.join("CHANNEL NAME HERE")
        print('Joining channel %s' % self.channel)

    def on_pubmsg(self, conn, irc_event):
        print("NEW MESSAGE!")
        nickname = irc_event.source.split('!')[0]
        channel = irc_event.target
        if nickname == self.announcer and channel == self.channel:
            print("Message belongs to correct user")
            conn.execute_delayed(1, self.process_message, (nickname, channel))
        else:
            print("Message does not belong to correct user")
        if channel not in self.line_cache:
            self.line_cache[channel] = {}
        if nickname not in self.line_cache[channel]:
            self.line_cache[channel][nickname] = []
        self.line_cache[channel][nickname].append(irc_event.arguments[0])
        # if len(self.line_cache[channel][nickname]) == 1:
        #     # Schedule a parse of the message in 1 second (for multilines)
        #     conn.execute_delayed(1, self.process_message, (nickname, channel))

    def on_privmsg(self, conn, irc_event):
        print("Private message received")
        print(irc_event)
        nickname = irc_event.source.split('!')[0]
        channel = irc_event.target
        if nickname == self.announcer and channel == self.channel:
            print("Private message received from announcer")
            conn.execute_delayed(1, self.process_message, (nickname, channel))
        else:
            print("Private message received from random user")
        if channel not in self.line_cache:
            self.line_cache[channel] = {}
        if nickname not in self.line_cache[channel]:
            self.line_cache[channel][nickname] = []
        self.line_cache[channel][nickname].append(irc_event.arguments[0])

    def process_message(self, nickname, channel):
        """
        Pops lines from the line cache and passes them to be parsed
        :param nickname: Nickname of who sent hte message
        :param channel: Channel where the message originated from
        :return: None
        """
        refresh()
        # draw()
        # clock()
        lines = u'\n'.join(self.line_cache[channel][nickname])
        self.line_cache[channel][nickname] = []
        app = Qt.QApplication(sys.argv)
        systemtray_icon = Qt.QSystemTrayIcon(app)
        systemtray_icon.show()
        filename = lines.split('||')[0]
        if "Freeleech" in lines:
            systemtray_icon.showMessage('New FILE Uploaded to %s' %site, filename)
            winsound.PlaySound('freeleech.wav', winsound.SND_FILENAME)
        else:
            systemtray_icon.showMessage('New FILE Uploaded to %s' % site, filename)
        print(channel)
        print(nickname + ": " + lines)

gui = GUI()

