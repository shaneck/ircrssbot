## IRC bot and RSS Scraper ##

### What it does

* The IRC bot waits in a user defined IRC channel for a specific user to announce that a file has been uploaded to the specified website.
* Then the RSS Scraper is triggered to scrape information about the file and display it in a GUI